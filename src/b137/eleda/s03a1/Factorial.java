package b137.eleda.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        System.out.println("Factorial\n");

        // Activity:
        // Create a Java program that accepts an integer and computes for
        // the factorial value and displays it to the console.

        int number;
        System.out.println("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        scanner.close();
        long fact = 1;
        int i = 1;
        while(i<=number)
        {
            fact = fact * i;
            i++;
        }
        System.out.println("Factorial of "+number+" is: "+fact);
    }
}
